#ASW TTWS 

The ASW TTWS (TimeTableWebService), is a project which aims to provide the web-facing ASW services as
an easy to use API. 
The current version is always hosted at http://ttws.notepass.de/
 
Currently the timetable can be accessed as ICAL and JSON and the marks can be accessed as JSON

Planned features:
- Access all available courses (JSon)
- Provide client Implementations (JS, Java, C#)

TODOs:
- Use Spring-Boot as Web-Server
- Enhance caching
- Clean up source code
- Write "How to set up" guide
- Use actual logging framework