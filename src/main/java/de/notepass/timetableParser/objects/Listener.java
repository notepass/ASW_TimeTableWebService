package de.notepass.timetableParser.objects;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by kim on 19.02.2016.
 */
public class Listener {
    private static HashMap<String, TimetableParsedListener> listener = new HashMap<>();

    public static synchronized void add(String url, TimetableParsedListener alistener) {
        listener.put(url, alistener);
    }

    public static synchronized void parsed(String url, Timetable t) {
        TimetableParsedListener l = listener.get(url);
        if (l != null) {
            l.parsed(t);
            listener.remove(url);
        }
    }
}
