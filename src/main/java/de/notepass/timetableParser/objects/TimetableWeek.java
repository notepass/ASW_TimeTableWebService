package de.notepass.timetableParser.objects;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Created by kim on 18.02.2016.
 */
public class TimetableWeek implements Iterable {
    private ArrayList<TimetableDay> days = new ArrayList<TimetableDay>();
    private Date startDate;
    private Date endDate;

    public void addDay(TimetableDay day) {
        days.add(day);

        if (startDate == null) {
            startDate = day.getDate();
        }

        if (endDate == null) {
            endDate = day.getDate();
        }

        if (startDate.compareTo(day.getDate()) > 0) {
            startDate = day.getDate();
        }

        if (endDate.compareTo(day.getDate()) < 0) {
            endDate = day.getDate();
        }
    }

    public Iterator iterator() {
        return days.iterator();
    }

    public void forEach(Consumer action) {
        days.forEach(action);
    }

    public Spliterator spliterator() {
        return days.spliterator();
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public ArrayList<TimetableDay> getDays() {
        return days;
    }
}
