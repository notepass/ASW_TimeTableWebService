package de.notepass.timetableParser.objects;

import java.util.ArrayList;

/**
 * Created by kim on 29.02.2016.
 */
public class Marks {
    private ArrayList<Mark> marks = new ArrayList<>();

    public ArrayList<Mark> getMarks() {
        return marks;
    }
}
