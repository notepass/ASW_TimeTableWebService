package de.notepass.timetableParser.objects;

/**
 * Created by kim on 19.02.2016.
 */
public interface TimetableParsedListener {
    public void parsed(Timetable t);
}
