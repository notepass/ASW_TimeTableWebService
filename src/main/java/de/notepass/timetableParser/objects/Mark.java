package de.notepass.timetableParser.objects;

/**
 * Created by kim on 29.02.2016.
 */
public class Mark {
    private String examName;
    private String reachedPoints;
    private String classAverage;

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public String getReachedPoints() {
        return reachedPoints;
    }

    public void setReachedPoints(String reachedPoints) {
        this.reachedPoints = reachedPoints;
    }

    public String getClassAverage() {
        return classAverage;
    }

    public void setClassAverage(String classAverage) {
        this.classAverage = classAverage;
    }
}
