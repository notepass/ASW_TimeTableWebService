package de.notepass.timetableParser.objects;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Created by kim on 18.02.2016.
 */
public class Timetable implements Iterable {
    private ArrayList<TimetableWeek> weeks = new ArrayList<TimetableWeek>();
    private Date startDate;
    private Date endDate;

    public void addWeek(TimetableWeek week) {
        weeks.add(week);

        if (startDate == null) {
            startDate = week.getStartDate();
        }

        if (endDate == null) {
            endDate = week.getEndDate();
        }

        if (startDate.compareTo(week.getStartDate()) > 0) {
            startDate = week.getStartDate();
        }

        if (endDate.compareTo(week.getEndDate()) < 0) {
            endDate = week.getEndDate();
        }
    }

    public Iterator iterator() {
        return weeks.iterator();
    }

    public void forEach(Consumer action) {
        weeks.forEach(action);
    }

    public Spliterator spliterator() {
        return weeks.spliterator();
    }

    public ArrayList<TimetableWeek> getWeeks() {
        return weeks;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }
}
