package de.notepass.timetableParser.objects;

import de.notepass.timetableParser.CalcUtil;
import de.notepass.timetableParser.Constant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Pattern;

/**
 * Created by kim on 18.02.2016.
 */
public class TimetableDay implements Iterable {
    private Date date;
    private ArrayList<TimetableHour> hours = new ArrayList<TimetableHour>();

    public void fromRawData(String[] date, String[][] hours) throws ParseException {
        int dateLeftBound = Float.valueOf(date[Constant.X]).intValue();
        int dateRightBound = Float.valueOf(date[Constant.Y]).intValue();
        this.date = new SimpleDateFormat("E, dd.MM.yyyy", Locale.GERMANY).parse(date[Constant.DATA_POS_INDEX]);

        for (String[] hour:hours) {
            int hourLeftBound = Float.valueOf(hour[Constant.X]).intValue();
            int hourRightBound = Float.valueOf(hour[Constant.Y]).intValue();

            if (CalcUtil.isBetween(dateLeftBound, dateRightBound, hourLeftBound, hourRightBound)) {
                String hourData = hour[Constant.DATA_POS_INDEX]; //13:00 - 14:30 UhrVorlesungWIMATNK: 1.07

                //TODO: More checks for ArrayIndexOutOfBounds
                String[] data = hourData.split(Pattern.quote("<br>"));
                String[] times = data[0].trim().split(Pattern.quote("-"));
                String startTime = times[0].trim();
                String endTime = times[1].trim();
                endTime = endTime.substring(0, endTime.indexOf("Uhr")).trim();
                String type = data[1].trim();
                String subject = data[2].trim();
                String room = "";
                String location = "";
                if (data.length > 3) {
                    room = data[3].trim();
                    String[] data2 = room.split(":");
                    if (data2.length > 1) {
                        room = data2[1].trim();
                        location = data2[0].trim();
                    }
                }
                TimetableHour tthour = new TimetableHour(new SimpleDateFormat("HH:mm", Locale.GERMANY).parse(startTime), new SimpleDateFormat("HH:mm", Locale.GERMANY
                ).parse(endTime), subject, room, location, type);
                this.hours.add(tthour);
            }
        }
    }

    public Date getDate() {
        return date;
    }

    public void addHour(TimetableHour hour) {
        hours.add(hour);
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Iterator iterator() {
        return hours.iterator();
    }

    public void forEach(Consumer action) {
        hours.forEach(action);
    }

    public Spliterator spliterator() {
        return hours.spliterator();
    }

    public ArrayList<TimetableHour> getHours() {
        return hours;
    }
}
