package de.notepass.timetableParser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import de.notepass.timetableParser.objects.Mark;
import de.notepass.timetableParser.objects.Marks;
import de.notepass.timetableParser.objects.Timetable;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by khayo15 on 29.02.2016.
 */
public class MarksWebservice implements HttpHandler {
    private static URL url = DownloadUtil.INSTANCE.urlFromString("http://download.asw-berufsakademie.de/cgi-bin/erg/klausuren_linn.pl");
    private static final String HELP_TEXT = "Marks webservice:\n" +
            "  endpoints:\n" +
            "    /marks/get:\n" +
            "      Retrives data\n" +
            "      Parameters:\n" +
            "        Required:\n" +
            "          username/<USERNAME> (e.g. XXX99.IF000000)\n" +
            "          password/<PASSWORD>\n" +
            "        Optional:\n" +
            "          none\n" +
            "      Example call:\n" +
            "        http://127.0.0.1:54781/marks/get/username/XXX99.IF000000/password/password\n" +
            "      Returns:\n" +
            "        JSon string\n" +
            "        Class format:\n" +
            "          Marks:\n" +
            "            Mark[] marks\n" +
            "          Mark:\n" +
            "            String examName\n" +
            "            String reachedPoints\n" +
            "            String classAverage";

    public void handle(HttpExchange t) throws IOException {
        MarksRequestResult result = new MarksRequestResult();
        Marks marks = new Marks();
        byte[] data = new byte[0];
        try {
            //System.out.println(new SimpleDateFormat("[dd-MM-yyyy-HH-mm]").format(new Date(System.currentTimeMillis()))+" MarksWebservice: Got request from "+t.getRemoteAddress().getHostName()+". URI: "+t.getRequestURI().toString());
            System.out.println(new SimpleDateFormat("[dd-MM-yyyy-HH-mm]").format(new Date(System.currentTimeMillis()))+" MarksWebservice: Got request from "+t.getRemoteAddress().getHostName()+".");
            OutputStream os = t.getResponseBody();
            try {
                HashMap<String, String> params = parseParams(URLDecoder.decode(t.getRequestURI().toString(), "UTF-8").split("/"));
                result.setErrorMessage("ERROR: Unknown error");
                ArrayList<String> contentType = new ArrayList<String>();
                contentType.add("text/plain; charset=utf-8");
                ArrayList<String> charset = new ArrayList<>();
                charset.add("utf-8");
                t.getResponseHeaders().put("content-type", contentType);
                t.getResponseHeaders().put("charset", charset);
                String action = params.get("action");
                if (action == null) {
                    action = "";
                }

                if (action.toLowerCase().equals("get")) {
                    if (params.get("username") != null && params.get("password") != null) {
                        try {
                            Document doc = Jsoup.parse(DownloadUtil.INSTANCE.getAsString(url, params.get("username"), params.get("password")));
                            Elements tableRows = doc.select("html > body > div > table > tbody > tr");
                            for (int i=1;i<tableRows.size()-1;i++) {
                                Elements tableColumns = tableRows.get(i).select("td");
                                Mark mark = new Mark();
                                for (int j=0;j<tableColumns.size();j++) {
                                    switch (j) {
                                        case 0:
                                            mark.setExamName(tableColumns.get(j).text());
                                            break;
                                        case 1:
                                            mark.setReachedPoints(tableColumns.get(j).text());
                                            break;
                                        case 2:
                                            mark.setClassAverage(tableColumns.get(j).text());
                                            break;
                                    }
                                }
                                marks.getMarks().add(mark);
                                result.setMarks(marks);
                                result.setSuccessful(true);
                            }
                        } catch(Exception e) {
                            result.setErrorMessage("ERROR: Could not load marks from ASW-Website. Please check username and password.");
                            e.printStackTrace();
                        }
                    } else {
                        result.setErrorMessage("ERROR: username or password missing");
                    }

                    String returnMessage = new Gson().toJson(result);
                    data = returnMessage.getBytes(Charset.forName("UTF-8"));
                } else {
                    data = HELP_TEXT.getBytes("UTF-8");
                }

                t.sendResponseHeaders(200, data.length);
                os.write(data);
                os.close();
            } catch (Exception e) {
                e.printStackTrace();
                t.sendResponseHeaders(500, "".length());
                os.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private HashMap<String, String> parseParams(String[] data) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (data.length > 2) {
            params.put("action", data[2]);
        }
        for (int i=3;i<data.length-1;i+=2) {
            params.put(data[i], data[i+1]);
        }
        return params;
    }
}
