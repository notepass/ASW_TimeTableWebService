package de.notepass.timetableParser;

import de.notepass.timetableParser.objects.Listener;
import de.notepass.timetableParser.objects.Timetable;
import de.notepass.timetableParser.objects.TimetableParsedListener;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import static de.notepass.timetableParser.FxUI.*;

/**
 * Created by kim on 19.02.2016.
 */
public class TimetableWebAdapter {
    public static Timetable parse(final String clazz, final int block) {
        final String url = String.format("http://www.asw-berufsakademie.de/fileadmin/download/download/Sked%%20Stundenplan/Studium/%s-%d.%%20Block.html", clazz, block);
        final Timetable[] tt = {null};
        final String id = UUID.randomUUID().toString();
        Listener.add(id, new TimetableParsedListener() {
            public void parsed(Timetable t) {
                tt[0] = t;
            }
        });
        Platform.runLater(new Runnable() {
            public void run() {
                WebView wv = new WebView();
                WebEngine engine = wv.getEngine();
                //engine.load("file:///C:/Users/kim/Desktop/timetableParser_/example.html");
                System.out.println(new SimpleDateFormat("[dd-MM-yyyy-HH-mm] ").format(new Date(System.currentTimeMillis()))+"Serving request for "+url+" with rid "+id);
                engine.load(url);
                //engine.load("file:///home/kim/WIA18-2. Block.html");

                engine.getLoadWorker().stateProperty().addListener(
                        new ChangeListener<Worker.State>() {
                            public void changed(ObservableValue<? extends Worker.State> ov, Worker.State oldState, Worker.State newState) {
                                if (newState == Worker.State.SUCCEEDED) {
                                    JSObject win = (JSObject) engine.executeScript("window");
                                    win.setMember("app", new BrowserBridge());
                                    engine.executeScript("tables = document.evaluate(\"//table[@style='margin-top:6pt']\", document);\n" +
                                            "currentTable = tables.iterateNext();\n" +
                                            "tablePositionObjects = [];\n" +
                                            "\n" +
                                            "\n" +
                                            "while (currentTable !== null) {\n" +
                                            "    dates = currentTable.getElementsByClassName('t');\n" +
                                            "    tablePositionObject = {};\n" +
                                            "    tablePositionObject.dates = [];\n" +
                                            "    tablePositionObject.hours = [];\n" +
                                            "\n" +
                                            "    for (datesPos=0;datesPos<dates.length;datesPos++) {\n" +
                                            "      tmp = dates[datesPos];\n" +
                                            "      var rect = tmp.getBoundingClientRect();\n" +
                                            "      tablePositionObject.dates.push([rect.top, rect.right, rect.bottom, rect.left, tmp.innerHTML]);\n" +
                                            "    }\n" +
                                            "\n" +
                                            "    currentHours = currentTable.getElementsByClassName('v');\n" +
                                            "    for (currentHour=0;currentHour<currentHours.length;currentHour++) {\n" +
                                            "      tmp = currentHours[currentHour];\n" +
                                            "      var rect = tmp.getBoundingClientRect();\n" +
                                            "      tablePositionObject.hours.push([rect.top, rect.right, rect.bottom, rect.left, tmp.innerHTML]);\n" +
                                            "    }\n" +
                                            "\n" +
                                            "    tablePositionObjects.push(tablePositionObject);\n" +
                                            "\n" +
                                            "    currentTable = tables.iterateNext();\n" +
                                            "}\n" +
                                            "\n" +
                                            "app.parse(JSON.stringify(tablePositionObjects), \""+id+"\" );\n");
                                    engine.getLoadWorker().stateProperty().removeListener(this);
                                }
                            }
                        });
            }
        });

        int i=0;
        while (tt[0] == null) {
            i++;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (i > 100) {
                break;
            }
        }

        return tt[0];
    }
}
