package de.notepass.timetableParser;

/**
 * Created by kim on 18.02.2016.
 */
public class BrowserBridgeObject {
    private String[][] dates;
    private String[][] hours;

    public String[][] getDates() {
        return dates;
    }

    public void setDates(String[][] dates) {
        this.dates = dates;
    }

    public String[][] getHours() {
        return hours;
    }

    public void setHours(String[][] hours) {
        this.hours = hours;
    }
}
