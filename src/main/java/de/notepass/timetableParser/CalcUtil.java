package de.notepass.timetableParser;

/**
 * Created by kim on 19.02.2016.
 */
public class CalcUtil {
    /**
     * Example of element to check
     * <pre>
     *     yo-------------xo
     *      yi-----------xi
     * </pre>
     * @param xo x-cord of outer element
     * @param yo y-cord of outer element
     * @param xi x-cord of inner element
     * @param yi y-cord of inner element
     * @return
     */
    public static boolean isBetween(int xo, int yo, int xi, int yi) {
        return xo <= xi && yo >= yi;
    }

    /**
     * Example of element to check
     * <pre>
     *     yo-------------xo
     *      yi-----------xi
     * </pre>
     * @param xo x-cord of outer element
     * @param yo y-cord of outer element
     * @param xi x-cord of inner element
     * @param yi y-cord of inner element
     * @param padding additional padding for xo(yo
     * @return
     */
    public static boolean isBetween(int xo, int yo, int xi, int yi, int padding) {
        return isBetween(xo+padding, yo+padding, xi, yi);
    }
}
