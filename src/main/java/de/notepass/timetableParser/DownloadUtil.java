package de.notepass.timetableParser;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

/**
 * Created by kim on 29.02.2016.
 */
public class DownloadUtil {
    public static final DownloadUtil INSTANCE = new DownloadUtil();

    public String getAsString(URL url, String username, String password) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        if (username != null && password != null) {
            String encoding = new String(java.util.Base64.getEncoder().encode((username + ":" + password).getBytes("UTF-8")), "UTF-8");
            connection.setRequestProperty  ("Authorization", "Basic " + encoding);
        }

        connection.setRequestMethod("GET");
        connection.setDoOutput(true);
        InputStream content = (InputStream)connection.getInputStream();
        BufferedReader in   =
                new BufferedReader (new InputStreamReader(content));
        StringBuilder buffer = new StringBuilder();
        String line;
        while ((line = in.readLine()) != null) {
            buffer.append(line);
        }

        return buffer.toString();
    }

    public URL urlFromString(String s) {
        try {
            return new URL(s);
        } catch (MalformedURLException e) {
            return null;
        }
    }
}
