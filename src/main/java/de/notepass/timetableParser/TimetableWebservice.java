package de.notepass.timetableParser;

import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import de.notepass.timetableParser.objects.Timetable;
import de.notepass.timetableParser.objects.TimetableDay;
import de.notepass.timetableParser.objects.TimetableHour;
import de.notepass.timetableParser.objects.TimetableWeek;
import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.model.*;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.parameter.Value;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Version;
import net.fortuna.ical4j.util.UidGenerator;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by kim on 19.02.2016.
 */
public class TimetableWebservice implements HttpHandler {
    public static final String OUTPUT_FORMAT_JSON = "JSON";
    public static final String OUTPUT_FORMAT_ICAL = "ICAL";
    private static Cache cache = CacheManager.getInstance().getCache("ttws");
    private static int cacheTTL = 3600; //1h
    private static int cacheRenewBracePeriod = 60; //Time in seconds in which the cache will need to rebuild
    private static String aswAddress = "Zum Eisenwerk 2\n" +
            "66538 Neunkirchen";

    private static final String HELP_STRING = "Timetable Webservice:\n" +
            "endpoints:\n" +
            "/timetable/get:\n" +
            "  Retrives data\n" +
            "  Parameters:\n" +
            "    Required:\n" +
            "      class/<CLASSNAME> (e.g. WIA18)\n" +
            "    Optional:\n" +
            "      block/<BLOCK> (e.g. 2)\n" +
            "      outputFormat/<JSON|ICAL>\n" +
            "      removeEmptyWeeks/<boolean>\n" +
            "      dateTimeFormat/<String> (e.g. dd-MM-yyyy-HH-mm or ISO-8601, Reference: https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html) - Only works in JSON export\n" +
            "    Example call:\n" +
            "      /timetable/get/class/WIA18/block/2\n" +
            "    Returns:\n" +
            "      JSon string\n" +
            "      Class format:\n" +
            "        class TimetableRequestResult:\n" +
            "          boolean successful\n" +
            "          String errorMessage\n" +
            "          Timetable timetable\n" +
            "        class Timetable:\n" +
            "          TimetableWeek[] weeks;\n" +
            "          Date startDate;\n" +
            "          Date endDate;\n" +
            "        class TimetableWeek:\n" +
            "          TimetableDay[] days;\n" +
            "          Date startDate;\n" +
            "          Date endDate;\n" +
            "        class TimetableDay:\n" +
            "          TimetableHour[] hours;\n" +
            "          Date date;\n" +
            "        class TimetableHour:\n" +
            "          Date startTime;\n" +
            "          Date endTime;\n" +
            "          String subject;\n" +
            "          String room;\n" +
            "          String location;\n" +
            "          String type;\n" +
            "      Misc:\n" +
            "        Date retunred in format dd-MM-yyyy-HH-mm (e.g. 27-02-2016-00-00)\n" +
            "        Because of internal workings, all \"Dates\" contin date and time. But only the date or time is set! (e.g.: In TimetableWeek only the Dates are set, because times are meaningless [Example value: 27-02-2016-00-00]. In TimetableHour only the time is filled, but dates are left to the default value [example value: 01-01-1970-18-15].)";

    public void handle(HttpExchange t) throws IOException {
        load(t, true, true, true);
    }

    private void load(HttpExchange t, boolean allowCacheLoad, boolean sendResponse, boolean autoRenewCache) {
        TimetableRequestResult requestResult = new TimetableRequestResult();
        String returnMessage = "";
        try {
            System.out.println(new SimpleDateFormat("[dd-MM-yyyy-HH-mm] ").format(new java.util.Date(System.currentTimeMillis()))+"TimetableWebservice: Got request from "+t.getRemoteAddress().getHostName()+". URI: "+t.getRequestURI().toString());
            OutputStream os = t.getResponseBody();
            try {
                HashMap<String, String> params = parseParams(URLDecoder.decode(t.getRequestURI().toString(), "UTF-8").split("/"));
                requestResult.setErrorMessage("ERROR: Unknown error");
                requestResult.setSuccessful(false);
                requestResult.setSuccessful(false);
                ArrayList<String> contentType = new ArrayList<String>();
                contentType.add("text/plain; charset=utf-8");
                ArrayList<String> charset = new ArrayList<>();
                charset.add("utf-8");
                t.getResponseHeaders().put("content-type", contentType);
                t.getResponseHeaders().put("charset", charset);
                String action = params.get("action");
                if (action == null) {
                    action = "";
                }
                String outputFormat = getParamValue("outputFormat", params, "JSON");
                outputFormat = outputFormat.toUpperCase();

                if (allowCacheLoad) {
                    if (cache.get(t.getRequestURI().toString()) != null) {
                        byte[] data = (byte[]) cache.get(t.getRequestURI().toString()).getObjectValue();
                        if (!action.toLowerCase().equals("get")) {
                            data = HELP_STRING.getBytes("UTF-8");
                        }

                        if (sendResponse) {
                            t.sendResponseHeaders(200, data.length);
                        }

                        if (sendResponse) {
                            os.write(data);
                            os.close();
                        }
                        return;
                    }
                }

                if (action.toLowerCase().equals("get")) {
                    String clazz = params.get("class");
                    boolean removeEmptyWeeks = params.get("removeEmptyWeeks")==null?false:Boolean.valueOf(params.get("removeEmptyWeeks"));
                    int block = -1;
                    try {
                        block = Integer.valueOf(params.get("block"));
                    } catch (Exception e) {

                    }

                    if (clazz != null) {
                        Timetable timetable;
                        if (block > 0) {
                            timetable = TimetableWebAdapter.parse(clazz, block);
                        } else {
                            timetable = TimetableWebAdapter.parse(clazz, 1);
                            for (int i=2;i<=10;i++) {
                                Timetable temp = TimetableWebAdapter.parse(clazz, i);
                                if (temp != null && temp.getWeeks() != null && temp.getWeeks().size() > 0) {
                                    timetable.getWeeks().addAll(temp.getWeeks());
                                }
                            }
                        }
                        if (removeEmptyWeeks) {
                            for (int i = 0; i < timetable.getWeeks().size(); i++) {
                                if (timetable.getWeeks().get(i).getStartDate() == null) {
                                    timetable.getWeeks().remove(i);
                                }
                            }
                        }

                        requestResult.setTimetable(timetable);
                        requestResult.setSuccessful(timetable != null && !timetable.getWeeks().isEmpty());

                    } else {
                        requestResult.setErrorMessage("ERROR: Class name not set or block not set/invalid");
                        requestResult.setSuccessful(false);
                    }
                }

                switch (outputFormat) {
                    case OUTPUT_FORMAT_JSON:
                        GsonBuilder builder = new GsonBuilder();
                        String dateTimeFormat = "dd-MM-yyyy-HH-mm";
                        if (params.get("dateTimeFormat") != null) {
                            dateTimeFormat = params.get("dateTimeFormat");
                            if (dateTimeFormat.toUpperCase().equals("ISO-8601")) {
                                dateTimeFormat = "yyyy-MM-dd'T'HH:mm+0100";
                            }
                        }
                        builder.setDateFormat(dateTimeFormat);
                        returnMessage = builder.create().toJson(requestResult);
                        break;

                    case OUTPUT_FORMAT_ICAL:
                        if (requestResult.isSuccessful()) {
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            Calendar calendar = new Calendar();
                            calendar.getProperties().add(new ProdId("-//Kim Hayo//Cached TTWS via iCal4j 1.0//DE"));
                            calendar.getProperties().add(Version.VERSION_2_0);
                            calendar.getProperties().add(CalScale.GREGORIAN);
                            TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
                            TimeZone timezone = registry.getTimeZone("Europe/Berlin");
                            VTimeZone tz = timezone.getVTimeZone();
                            calendar.getComponents().add(tz);


                            for (TimetableWeek week:requestResult.getTimetable().getWeeks()) {
                                for (TimetableDay day:week.getDays()) {
                                    for (TimetableHour hour:day.getHours()) {
                                        SimpleDateFormat fuckIt = new SimpleDateFormat("dd.MM.yyyy|HH:mm");
                                        String tmp = fuckIt.format(day.getDate());
                                        String tmp2 = fuckIt.format(hour.getStartTime());
                                        String tmp3 = fuckIt.format(hour.getEndTime());
                                        java.util.Date startDate = fuckIt.parse(tmp.substring(0, tmp.indexOf('|')) + tmp2.substring(tmp2.indexOf('|')));
                                        java.util.Date endDate = fuckIt.parse(tmp.substring(0, tmp.indexOf('|')) + tmp3.substring(tmp2.indexOf('|')));

                                        //long startDateL = day.getDate().getTime()+hour.getStartTime().getTime();
                                        //long endDateL = day.getDate().getTime()+hour.getEndTime().getTime();
                                        //java.util.Date startDate = new java.util.Date(startDateL);
                                        //java.util.Date endDate = new java.util.Date(endDateL);
                                        VEvent tthourIcal = new VEvent(
                                                new DateTime(startDate),
                                                new DateTime(endDate),
                                                (hour.getType().toLowerCase().equals("klausur")?"Klausur: ":"")+hour.getSubject()
                                        );
                                        tthourIcal.getProperties().add(new Location(aswAddress+"\nRaum: "+hour.getRoom()));
                                        calendar.getComponents().add(tthourIcal);
                                        UidGenerator uidGenerator = new UidGenerator("1");
                                        tthourIcal.getProperties().add(uidGenerator.generateUid());
                                    }
                                }
                            }

                            CalendarOutputter outputter = new CalendarOutputter();
                            outputter.output(calendar, baos);
                            returnMessage = new String(baos.toByteArray(), "UTF-8");
                        }
                        break;
                }


                byte[] data = returnMessage.getBytes(Charset.forName("UTF-8"));
                if (!action.toLowerCase().equals("get")) {
                    data = HELP_STRING.getBytes("UTF-8");
                }

                if (sendResponse) {
                    t.sendResponseHeaders(200, data.length);
                }

                Element e = new Element(t.getRequestURI().toString(), data);
                e.setTimeToLive(cacheTTL);
                cache.put(e);

                if (autoRenewCache) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            while (true) {
                                try {
                                    Thread.sleep((cacheTTL - cacheRenewBracePeriod) * 1000);
                                } catch (InterruptedException e1) {
                                    e1.printStackTrace();
                                    break; //To not accidently DoS the timetable website
                                }

                                load(t, false, false, false);
                            }
                        }
                    }, "Renew_Cache_" + t.getRequestURI().toString()).start();
                }

                if (sendResponse) {
                    os.write(data);
                    os.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (sendResponse) {
                    t.sendResponseHeaders(500, "".length());
                }
                if (sendResponse) {
                    os.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private HashMap<String, String> parseParams(String[] data) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (data.length > 2) {
            params.put("action", data[2]);
        }
        for (int i=3;i<data.length-1;i+=2) {
            params.put(data[i], data[i+1]);
        }
        return params;
    }

    private String getParamValue(String name, HashMap<String, String> data, String fallback) {
        String tmp = data.get(name);
        if (tmp != null && !tmp.isEmpty()) {
            return tmp;
        } else {
            return fallback;
        }
    }
}
