package de.notepass.timetableParser;

/**
 * Created by kim on 19.02.2016.
 */
public class Constant {
    public final static int TOP_LEFT_POS_INDEX = 0;
    public final static int TOP_RIGHT_POS_INDEX = 1; //TOP_RIGHT
    public final static int BOTTOM_LEFT_POS_INDEX = 2;
    public final static int BOTTOM_RIGHT_POS_INDEX = 3; //TOP_LEFT
    public final static int DATA_POS_INDEX = 4;
    public final static int DEFAULT_PADDING = 10;

    public final static int X = 3;
    public final static int Y = 1;
}
