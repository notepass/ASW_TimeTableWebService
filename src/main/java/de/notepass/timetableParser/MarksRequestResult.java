package de.notepass.timetableParser;

import de.notepass.timetableParser.objects.Marks;

/**
 * Created by kim on 29.02.2016.
 */
public class MarksRequestResult {
    private boolean successful = false;
    private String errorMessage = "";
    private Marks marks = null;

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Marks getMarks() {
        return marks;
    }

    public void setMarks(Marks marks) {
        this.marks = marks;
    }
}
