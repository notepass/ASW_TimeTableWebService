package de.notepass.timetableParser;

import de.notepass.timetableParser.objects.Timetable;

import java.sql.Time;

/**
 * Created by khayo15 on 29.02.2016.
 */
public class TimetableRequestResult {
    private boolean successful = true;
    private String errorMessage = "";
    private Timetable timetable = null;

    public TimetableRequestResult() {

    }

    public TimetableRequestResult(boolean successful, String errorMessage, Timetable timetable) {
        setErrorMessage(errorMessage);
        setSuccessful(successful);
        setTimetable(timetable);
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Timetable getTimetable() {
        return timetable;
    }

    public void setTimetable(Timetable timetable) {
        this.timetable = timetable;
    }
}
