package de.notepass.timetableParser;

import com.sun.net.httpserver.HttpServer;
import javafx.application.Application;
import net.sf.ehcache.CacheManager;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * Created by kim on 19.02.2016.
 */
public class Startup {
    public static void main(String[] args) {
        try {
            System.out.println("Starting. Version: 0.8");
            //Renderer
            new Thread(new Runnable() {
                public void run() {
                    Application.launch(FxUI.class);
                }
            }).start();

            CacheManager.getInstance().addCache("ttws");

            //Webservice
            new Thread(new Runnable() {
                public void run() {
                    HttpServer server = null;
                    try {
                        server = HttpServer.create(new InetSocketAddress(54781), 0);
                    } catch (IOException e) {
                        e.printStackTrace();
                        System.exit(1);
                    }
                    server.createContext("/timetable", new TimetableWebservice());
                    server.createContext("/marks", new MarksWebservice());
                    server.setExecutor(null); // creates a default executor
                    server.start();
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
