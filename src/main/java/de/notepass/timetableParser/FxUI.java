package de.notepass.timetableParser;

import com.google.gson.Gson;
import de.notepass.timetableParser.objects.Listener;
import de.notepass.timetableParser.objects.Timetable;
import de.notepass.timetableParser.objects.TimetableDay;
import de.notepass.timetableParser.objects.TimetableWeek;
import java.io.UnsupportedEncodingException;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import netscape.javascript.JSObject;

import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by kim on 17.02.2016.
 */
public class FxUI extends Application {

    public static void main(String[] args) {

    }

    @Override
    public void start(Stage primaryStage) throws Exception {

    }

    public static Timetable parse(String json) throws ParseException {
        Gson gson = new Gson();
        BrowserBridgeObject[] data = gson.fromJson(json, BrowserBridgeObject[].class);

        Timetable timetable = new Timetable();

        for (BrowserBridgeObject o:data) {
            TimetableWeek week = new TimetableWeek();

            for (String[] cdate:o.getDates()) {
                TimetableDay day = new TimetableDay();
                day.fromRawData(cdate, o.getHours());
                week.addDay(day);
            }

            timetable.addWeek(week);
        }

        return timetable;
    }
}
