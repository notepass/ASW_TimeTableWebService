package de.notepass.timetableParser;

import de.notepass.timetableParser.objects.Listener;

import java.text.ParseException;

/**
 * Created by kim on 23.02.2016.
 */
public class BrowserBridge {
    public void parse(String json, String url) {
        try {
            Listener.parsed(url, FxUI.parse(json));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}